# Find recently modified files

Find files in the current directory that were modified in the last 5 minutes:

```
find . -type f -mmin -5
```

